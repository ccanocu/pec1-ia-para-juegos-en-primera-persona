using UnityEngine;
using System.Collections;

public class MaquinaDeEstados : MonoBehaviour {

    public Estado EstadoPatrulla;
    public Estado EstadoIdle;
    public Estado EstadoInicial;

    private Estado estadoActual;

    void Start () {
        ActivarEstado(EstadoInicial);
	}

    public void ActivarEstado(Estado nuevoEstado)
    {
        if(estadoActual!=null) estadoActual.enabled = false;
        estadoActual = nuevoEstado;
        estadoActual.enabled = true;
    }

}
