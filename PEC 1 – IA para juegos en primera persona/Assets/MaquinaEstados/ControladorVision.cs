using UnityEngine;
using System.Collections;

public class ControladorVision : MonoBehaviour {

    public Transform Sensor;
    public float rangoVision = 0.5f;
    public Vector3 offset = new Vector3(0f, 0.2f, 0f);

    private ControladorNavMesh controladorNavMesh;

    void Awake()
    {
        controladorNavMesh = GetComponent<ControladorNavMesh>();
    }

    public bool PuedeVerAlJugador(out RaycastHit hit, bool mirarHaciaElJugador = false)
    {
        Vector3 vectorDireccion;
        if (mirarHaciaElJugador)
        {
            vectorDireccion = (controladorNavMesh.perseguirObjectivo.position + offset) - Sensor.position;
        }else
        {
            vectorDireccion = Sensor.forward;
        }

        return Physics.Raycast(Sensor.position, vectorDireccion, out hit, rangoVision) && hit.collider.CompareTag("Banco");
    }
}
