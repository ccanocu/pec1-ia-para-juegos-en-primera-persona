using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NevMeshController : MonoBehaviour
{
    public Transform[] goal;
    int llegada = 0;
    NavMeshAgent agente;

    void Start()
    {   
        agente = GetComponent<NavMeshAgent>();
        agente.autoBraking = true;
        agente.destination = goal[llegada].position;
    }

    void Update()
    {
        
        if(agente.remainingDistance <= 1 & llegada == 0)
        {   
            llegada = 1;   
            agente.destination = goal[llegada].position;
        }
        else if (agente.remainingDistance <= 1 & llegada == 1)
        {
            llegada = 2;
            agente.destination = goal[llegada].position;
        }
        else if (agente.remainingDistance <= 1 & llegada ==2 )
        {
            llegada = 3;            
            agente.destination = goal[llegada].position;
        }
        else if (agente.remainingDistance <= 1 & llegada ==3 )
        {
            llegada = 4;            
            agente.destination = goal[llegada].position;
        }
        else if (agente.remainingDistance <= 1 & llegada ==4 )
        {
            llegada = 5;            
            agente.destination = goal[llegada].position;

        }
        else if(agente.remainingDistance <= 1 & llegada == 5 )
            llegada = 0;            
            agente.destination = goal[llegada].position;
    }
}
