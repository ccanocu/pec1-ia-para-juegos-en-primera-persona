using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRunner : MonoBehaviour
{
    public GameObject pt;
    float speedRun;
    public float velocidadInicialGhost;
    public float velocidadSeguidor;

    private UnityEngine.AI.NavMeshAgent at;

    void Start()
    {
        at = GetComponent<UnityEngine.AI.NavMeshAgent>();
        at.speed = velocidadSeguidor;
        pt.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = velocidadInicialGhost;
    }

    void Update()
    {
        
        at.destination = pt.GetComponent<Transform>().position;

        if (at.remainingDistance >= 5)
            pt.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = -1;
        else if (at.remainingDistance < 5 )
            pt.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = velocidadInicialGhost;
    }
}
