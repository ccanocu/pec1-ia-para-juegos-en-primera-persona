using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class EstadoPersecucion : Estado
{
    private ControladorNavMesh controladorNavMesh;
    private ControladorVision controladorVision;
    private NavMeshAgent aget;

    float secsToNext=0.0f;


    private bool wait= true;
    private Vector3 pisiconGuardad = new Vector3(0,0,0);

    protected override void Awake()
    {
        base.Awake();
        aget = GetComponent<NavMeshAgent>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorVision = GetComponent<ControladorVision>();
        
	}

    void OnEnable()
    {
        RaycastHit hit; 

        controladorVision.PuedeVerAlJugador(out hit);
        Debug.Log("BB");

        if (hit.transform.position != pisiconGuardad)
        {
            controladorNavMesh.ActualizarPuntoDestinoNavMeshAgent(hit.transform.position);
            pisiconGuardad = hit.transform.position;
        }
        else
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoPatrulla);

  
        Debug.Log("AAA");
    }
	
	void Update () {

        if (aget.remainingDistance == 0 )
        {
            secsToNext += Time.deltaTime;  // T.dt is secs since last update
            
            if(secsToNext >= 10) {
                maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoPatrulla);
                aget.updateRotation = false;
                secsToNext = 0;
            }
        }
	}

}
