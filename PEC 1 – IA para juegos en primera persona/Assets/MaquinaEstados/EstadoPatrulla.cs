using UnityEngine;
using System.Collections;

public class EstadoPatrulla : Estado
{

    public Transform[] WayPoints;

    private ControladorNavMesh controladorNavMesh;
    private ControladorVision controladorVision;
    private UnityEngine.AI.NavMeshAgent aget;
    private int siguienteWayPoint;

    protected override void Awake()
    {
        base.Awake();
        aget = GetComponent<UnityEngine.AI.NavMeshAgent>();
        aget.updateRotation = false;
        aget.autoBraking = false;
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorVision = GetComponent<ControladorVision>();
    }
	
	// Update is called once per frame
	void Update () {
        // Ve al jugador?
        RaycastHit hit;
        if(controladorVision.PuedeVerAlJugador(out hit))
        {
            controladorNavMesh.perseguirObjectivo = hit.transform;
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoIdle);
            return;
        }

        if (controladorNavMesh.HemosLlegado())
        {
            siguienteWayPoint = (siguienteWayPoint + 1) % WayPoints.Length;
            ActualizarWayPointDestino();
        }
	}

    void OnEnable()
    {
        ActualizarWayPointDestino();
    }

    void ActualizarWayPointDestino()
    {
        controladorNavMesh.ActualizarPuntoDestinoNavMeshAgent(WayPoints[siguienteWayPoint].position);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Banco") && enabled)
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoIdle);
        }
    }
}
